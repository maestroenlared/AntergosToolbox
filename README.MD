# Antergos ToolBox
## Script creado por Maestro en la Red
---
#### Página web: [Maestro en la Red](http://maestro-en-lared.blogspot.com.es/)
---

### Índice

1. #### [Cómo ejecutar][1]
[1]: #c%C3%B3mo-ejecutar-1

2. #### Funciones
  * Actualizar y limpiar sistema
  * Ver uso de disco
  * Ver uso de RAM
  * Formatear dispositivo de almacenamiento
  * Copias de seguridad
  * Gestión de usuarios y grupos



### Cómo ejecutar
Para utilizar el script abrimos el terminal y nos posicionamos en la carpeta en la que tengamos Antergos ToolBox.
Tecleamos lo siguiente:

```[sh]
$ sudo chmod a+x AntergosToolbox.sh
$ sudo ./AntergosToolbox.sh
```
Otra opción sería:

```[bash]
$ sudo sh AntergosToolbox.sh
```

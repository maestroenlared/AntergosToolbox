Ver estado del repositorio, comprobar conflictos:<br>
`git log`<br>

Ver log de commits:<br>
`git log`<br>

Ver diferencias rama local vs master:<br>
`git diff NombreDelArchivo`<br>

Actualizar desde master sin perder los cambios en local:<br>
`git stash && git pull -r && git stash pop`<br>

Hacer commit y push (Actualizar el master desde local):<br>
`git add NombreDelArchivo && git commit -m "Descripcion" && git push`<br>

Descargar un archivo de cualquier commit a local sin reemplazar:<br>
`git cat-file -p hashDelCommit:NombreDelArchivo > NombredelArchivoaGuardar`<br>

Decargar una versión del commit y subir al master:<br>
`git revert --hard hashDelCommit && git push`

Sustituir local al commit que se quiera:<br>
`git reset --hard hashDelCommit`<br>

Ver todos los commits de un archivo:<br>
`git log --follow tickets.php`<br>

Cambiar de rama del repositorio:<br>
`git checkout nombreRama`<br>

Error git "fatal: git-write-tree: error building trees":<br>
`git reset --mixed`<br>

![Git CheatSheet](http://obaida.info/blog/wp-content/uploads/2015/03/git-transport-v1.png)
[
#!/bin/bash
#Definimos la función formateo para hacer un poco mas legible el Script
function formateo  {
  if [ $3 == 'mkfs.vfat' ]; then
    echo "Formateando " /dev/$1$2
    sleep 2s
    sudo $3 $4 $5 -n $6 /dev/$disco$particion
    echo Dispositivo formateado con exito a $7 con el nombre $6
  else
    echo "Formateando " /dev/$1$2
    sleep 2s
    sudo $3 -n $4 /dev/$disco$particion
    echo Dispositivo formateado con exito a $5 con el nombre $4
  fi
}
#sleep 4s
#sudo mount -t vfat /dev/sdb1 /mnt/
#sleep 2s
#echo "USB Formateada con exito a FAT 32 y renombrada como miusb"

clear
while :
do
echo "
+-+-+-+-+-+-+-+-+ +-+-+-+-+-+-+-+
|A|N|T|E|R|G|O|S| |T|O|O|L|B|O|X|
+-+-+-+-+-+-+-+-+ +-+-+-+-+-+-+-+"
##SCRIPT CREADO POR MAESTRO EN LA RED https://www.maestro-en-lared.blogspot.com.es, se permite la distribución del Script pero siempre reconociendo la autoria de Maestro en la Red##
sleep 2s
echo "SCRIPT CREADO POR MAESTRO EN LA RED"
echo "www.maestro-en-lared.blogspot.com.es"
echo "-------------------------------------"
sleep 4s
echo " Escoja una opcion "
echo "1. Actualizar y Limpiar Sistema"
echo "2. Ver uso de disco"
echo "3. Ver uso de RAM"
echo "4. Formatear Dispositivo de Almacenamiento"
echo "5. Copias de Seguridad"
echo "6. Gestión de Usuarios y Grupos"
echo "7. Salir"
echo -n "Seleccione una opcion [1 - 7]"
read opcion
case $opcion in
1) echo "Actualizar y Limpiar Sistema:";
sleep 6s
echo "Actualizando Sistema"
sleep 2s
sudo pacman -Syyu
sleep 2s
echo "Borrando la cache de pacman"
sudo pacman -Scc
sleep 2s
echo "Borrando los paquetes huérfanos"
sudo pacman -Rs $(pacman -Qtdq)
sleep 2s
echo "Vaciando todas las papeleras"
sudo rm -rf /home/*/.local/share/Trash/*/**
sudo rm -rf /root/.local/share/Trash/*/**
sleep 2s
echo "Borrando Archivos temporales"
rm -rf /tmp/* &> /dev/null
sleep 4s
echo "LA LIMPIEZA DE SU SISTEMA SE A REALIZADO CORRECTAMIENTE"
;;

2) echo "Uso de disco:";
df;;
3) echo "Uso de RAM:";
free;;
4) echo "Formatear Dispositivo:";

#USB --------------------------------------------------- USB#
echo "Imprimiendo tabla de particiones"
sleep 2s
sudo fdisk -l
sleep 2s
echo -e "\e[1;31mPor favor, asegurese de escribir correctamente el nombre de la unidad a formatear (i.e sda, sdb, sdc,..)"
echo -e "No haga nada sin leer la tabla de particiones y discos que se le muestra arriba\e[0m"
sleep 2s
echo "Escriba el nombre del disco a formatear:"
read disco
echo "Escriba  el numero de partición (1,2,3,...)"
read particion
echo "Desmontando " /dev/$disco$particion
sleep 2s
sudo umount /dev/$disco$particion
while :
do
echo "Elija el sistema de archivos a usar por su partición"
echo "1.FAT32"
echo "2.NTFS"
echo "3.Ext3"
echo "4.Ext4"
read n
case $n in
 1)formato='mkfs.vfat -F 32'
   f='FAT32'
 break
    ;;
 2)formato='mkfs.ntfs'
   f='NTFS'
 break
    ;;
 3)formato='mkfs.ext3'
   f='EXT3'
 break
   ;;
 4)formato='mkfs.ext4'
   f='EXT4'
 break
   ;;
 *) echo '\e[1;31mOpción errónea\e[0m'
esac
done
echo "Elija un nombre para asignar  a su dispositivo (Los nombres en minúscula pueden dar problemas)"
read label
echo "Vamos a formatear la siguiente partición"
sudo fdisk -l | grep $disco$particion
echo "Formato escogido: "$f
echo "Nombre escogido: "$label
sleep 1s
while :
do
echo "¿Esta seguro? (Y/N)"
read letra
case $letra in
    Y) formateo $disco $particion $formato $label $f
      break
    ;;
    y) formateo $disco $particion $formato $label $f
      break
    ;;
    N) break
    ;;
    n) break
esac
done

#USB --------------------------------------------------- USB#
;;


5) echo "Copias de Seguridad:";
#BACKUP----------------------------------------------------BACKUP#

#!/bin/bash
 clear
while :
do
echo " Escoja una opcion "
echo "1. Crear Backup de Software"
echo "2. Restaurar Backup de Software"
echo "3. Salir"
echo -n "Seleccione una opcion [1 - 3]"
read opcion
case $opcion in
1) echo "Crear Backup de Software:";
sleep 2s
echo "Creando Backup de Software..."
sleep 2s
sudo pacman -Qqe | grep -v "$(pacman -Qmq)" > pkglist
sleep 4s
echo "Backup de Software creado correctamente y guardado en la ubicación de ANTERGOS TOOLBOX"
sleep 4s
;;
2) echo "Restaurar Backup de Software:";
sleep 2s
echo "Restaurando Backup de Software..."
sleep 2s
sudo pacman -S $(cat pkglist)
sleep 4s
echo "Backup de Software Restaurado con exito"
sleep 4s
;;
3) echo "Gracias por usar ANTERGOS TOOLBOX";
exit 1;;
*) echo "$opc no es una opcion válida.";
echo "Presiona una tecla para continuar...";
read foo;;
esac
done
]
#BACKUP----------------------------------------------------BACKUP#
;;

6) echo "Gestión de Usuarios y Grupos:";
#GestiónUsuarios----------------------------------------GestiónUsuarios#
!/bin/bash
#Comprobamos que el usuario es root.
if [ $UID != 0 ]; then
 echo "No tienes los privilegios necesarios para ejecutar este script."
 echo "Debes ingresar como root, escribe \"su root\" sin las comillas."
 exit 1
fi

echo
echo "Gestión de Usuarios y Grupos en GNU/Linux"
echo

while [ "$option" != "8" ]
 do
 clear
  echo
  echo "Gestión de Usuarios y Grupos"
  echo " Escoja una opcion "
  echo " 1. Agregar un usuario al sistema."
  echo " 2. Cambiar la clave de acceso de un usuario."
  echo " 3. Editar la información personal de un usuario."
  echo " 4. Borrar a un usuario del sistema."
  echo " 5. Crear grupo."
  echo " 6. Agregar un usuario a un grupo."
  echo " 7. Borrar un grupo."
  echo " 8. Salir."
  echo
  echo -n "Seleccione una opcion [1 - 8]"
   read option
   case $option in
    1 )
     echo
     echo -n "Nombre del usuario a crear: "
     read username
     echo "Con acceso a la línea de comandos bash [Y/n]?"
     echo -n " Valor por defecto [n]: "
     read shell_access
     if [ "$shell_access" = "Y" ]; then
       TERMINAL='/bin/bash'
     else
      TERMINAL='/bin/false'
     fi
     echo "Crear el directorio de trabajo para el nuevo usuario [Y/n]?"
     echo -n " Valor por defecto [Y]: "
     read home_directory
     if [ "$home_directory" = "n" ]; then
      useradd $username -s $TERMINAL
     else
      useradd $username -m -s $TERMINAL
     fi
     echo
    ;;
    2 )
     echo
     echo -n "Nombre del usuario del que se desea cambiar la clave: "
     read username
     echo
     passwd $username
     echo
    ;;
    3 )
     echo
     echo -n "Nombre del usuario al cual cambiar la información personal: "
     read username
     echo
     chfn $username
     echo
    ;;
    4 )
     echo
     echo -n "Nombre del usuario a borrar: "
     read username
     if [ "$username" = "root" ]; then
      echo "Estúpido script, no puedes eliminar al Dios root"
      exit 0
     else
      echo
      echo "Desea borrar el directorio de trabajo y todo su contenido [Y/n]? "
      echo -n " Valor por defecto [n]: "
      read delete_home
     fi
     if [ "$delete_home" = "Y" ]; then
      userdel -r $username
     else
      userdel $username
     fi
     echo
    ;;
    5 )
     echo
     echo -n "Nombre del grupo a crear: "
     read group
     echo
     groupadd $group
     echo
    ;;
    6 )
     echo
     echo -n "Nombre del usuario para agregar al grupo: "
     read username
     echo -n "Nombre del del grupo para agregar el usuario: "
     read group
     echo
     groupadd $username $group
     echo
    ;;
   7 )
     echo -n "Nombre del grupo a borrar: "
     read group
     echo
     groupdel $group
     echo
    ;;
    *)
    ;;
   esac
  done
echo
exit 8
#GestiónUsuarios----------------------------------------GestiónUsuarios#
;;
7) echo "Gracias por usar ANTERGOS TOOLBOX";
sleep 2s
sudo clear && exit 1;;
*) echo "$opc no es una opcion válida.";
echo "Presiona una tecla para continuar...";
read foo;;
esac
done
]
